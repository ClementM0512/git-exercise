# Évaluation Git

- Prénom et nom : Clément Molinié, Aleksandr Vassilyev
- Commentaires : Les boss ;)

## Exercices

- 1 point - Fourcher et cloner le dépôt

- 1 point - Remplir votre nom ci-dessus 

- 1 point - Reprendre dans master le commit de la branche `equality_refinement` via `cherry-pick`

- 1 point - Défaire le commit `remove unused subtract functions`

- 1 point - Fusionner dans `master` la branche `doc/intro`

- 2 points - Fusionner dans `master` la branche `add_distance`

- 1 point - Pousser le nouveau `master`

- 2 points - Créer une branche sur le dernier `master`, créer une nouvelle fonction de calcul, la committer, et pousser cette nouvelle branche 

- 2 points - Rebaser la branche `add_tests` sur `master`
- 1 point - Pousser la branche `add_tests` sur le dépôt distant
- 2 points - Ouvrir une merge request à partir de cette branche `add_tests` sur le dépôt source


## Questions

- Donner l’auteur du commit qui a introduit la fonction `add` dans ce projet : Jalil Arfaoui à commit la fonction add

- Indiquer quelle commande utiliser, dans un projet Git,  pour changer le message du dernier commit:  git commit --amend -m "MSG"
- Quelle commande, inverse de `git add`, permet de retirer un fichier du stage (prochain commit) ? git reset <chemins>


- Donner le nom du créateur de Git : Linus Torvalds

- Indiquer la(les) différence(s) entre les commandes `git init` et `git clone` : Git init permet d'initialiser un nouveau dépôt git en local et 
git clone sert à cloner un repository dans un nouveau.

